*** Settings ***
Library  BuiltIn

*** Variables ***
${width}=  18
${height}=  4
${area}=  0

*** Test Cases ***
1. คำนวณพื้นที่สามเหลี่ยม
    ${area}=  TriangleArea  ${width}  ${height}
    Set Global Variable  ${area}

2. แสดงผล
    Log To Console  \nTriangleArea: ${area}

3. แสดงผลว่า Good Bye
    Hi

*** Keywords ***
TriangleArea
    [Arguments]  ${paraWidth}  ${paramHeight}
    ${respArea}=  Evaluate  ${paraWidth} * ${paramHeight} * 0.5
    [Return]  ${respArea}

Hi
    Log To Console  \Good bye
    Log To Console  \nHi
    Log To Console  \nThank you